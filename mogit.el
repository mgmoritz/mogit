;;; mogit.el --- Git utilities

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos Goularte Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((magit "20210227.1342"))
;; Keywords: git
;; URL: https://mo-profile.com

;;; Commentary:
;; This is package adds emacs lisp git utilities


(require 'magit)
(require 's)

(defun mogit--remote-get-url ()
  (interactive)
  (let ((current-remote (substring-no-properties (magit-get-current-remote))))
    (if current-remote
        (s-trim
         (shell-command-to-string
          (format "git remote get-url %s" current-remote)))
      (error "Not a git repository"))))

(provide 'mogit)
